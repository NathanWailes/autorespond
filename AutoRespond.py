#!/usr/bin/python
#AutoRespond should be used with the 32bit version of Python 3.35 because that's
#the only version I could find where all the necessary modules play nicely with
#each other when being converted to an executable.
"""This is the master file from which all the other files are called"""
import json
import time
from autoresponder import respond_to_new_ads
from GUI import createProfileIfNecessary
from user import User
from craigslist_scraper import CraigslistScraper

def main():
    """Automatically respond to Craigslist ads for approved users"""
    users = assemble_users()

    regions = ["washingtondc"]

    craigslist_scraper = CraigslistScraper()

    while True:
        #TODO: Add a section here that updates the list of ads.
        for region in regions:
            craigslist_scraper.add_listings("washingtondc", "jjj")
            listings = craigslist_scraper.get_listings()
            #This is totally unfinished. I'm just leaving it here as a start.

        for user in users:
            print("Examining " + user.username + "...")
            if user.payment_verifier.verifyPayment(user.username) == False:
                print(user.username + " hasn't paid. Skipping to next user.")
                continue
            else:
                #try:
                print("Attempting to respond to ads for " + user.username)
                respond_to_new_ads(user, listings)
                '''
                except:
                    print("Encountered a problem while responding to ads.\n" + \
                          "Skipping to the next user.")
                    traceback.print_exc()
                    continue #I should have it email me if something goes wrong.
                '''
        print("Waiting a minute - " + time.asctime())
        time.sleep(60)
    return

def assemble_users():
    """Load user data from json files"""
    users = []
    createProfileIfNecessary()
    with open('users.json') as list_of_users_file:
        users_by_region = json.load(list_of_users_file)
    for region in users_by_region:
        for username in users_by_region[region]:
            user = User(username, region)
            users.append(user)
    return users

if __name__ == "__main__":
    main()
