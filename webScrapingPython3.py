#Source - http://stackoverflow.com/questions/16782726/web-scraping-tutorial-using-python-3

import requests
from bs4 import BeautifulSoup
import re

def getEmailAddressFromCraigslistReplyPage(url):

    # spoof some headers so the request appears to be coming from a browser, not a bot
    headers = {
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5)",
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "accept-charset": "ISO-8859-1,utf-8;q=0.7,*;q=0.3",
        "accept-encoding": "gzip,deflate,sdch",
        "accept-language": "en-US,en;q=0.8",
    }
    # make the request to the search url, passing in the the spoofed headers.
    r = requests.get(url)  #, headers=headers)assign the response to a variable r
    # check the status code of the response to make sure the request went well
    if r.status_code != 200:
        print("request denied")
        return
    #else: #I don't need to print anything right now.
    #    print("scraping " + url + "\n")

    # convert the plaintext HTML markup into a DOM-like structure that we can search
    soup = BeautifulSoup(r.text)

    #works with bs4
    for link in soup.find_all('a'):
        p = re.compile('[\w\.-]+@[\w\.-]+')
        #m = re.match(".+", str(link))
        m = p.findall(str(link))
        if ("@" in m[0]) and ("." in m[0]):
            email_address = m[0]
            return email_address
        else:
            return False

def getUserInfo(url, email_address):

    # spoof some headers so the request appears to be coming from a browser, not a bot
    headers = {
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5)",
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "accept-charset": "ISO-8859-1,utf-8;q=0.7,*;q=0.3",
        "accept-encoding": "gzip,deflate,sdch",
        "accept-language": "en-US,en;q=0.8",
    }
    # make the request to the search url, passing in the the spoofed headers.
    address = url + "?email_address=" + email_address
    r = requests.get(address) #, headers=headers) #assign the response to a variable r
    # check the status code of the response to make sure the request went well
    if r.status_code != 200:
        print("request denied")
        return
    #else: #I don't need to print anything right now.
    #    print("scraping " + url + "\n")

    # convert the plaintext HTML markup into a DOM-like structure that we can search
    soup = BeautifulSoup(r.text)

    #the regexes below assume each bit of data will be followed by a "<br>"
    #otherwise the regexes won't match correctly.
    user_info = {}

    #get user_id
    user_id = 0
    p = re.compile('(?:id: )\w+')
    m = p.findall(str(soup))
    if len(m) > 0:
        if ("id" in m[0] and len(m[0]) >= 5): #ex: m[0] = "id: 5"
            user_id = m[0] #example: user_id = "id: 5"
            user_id = user_id[4:] #trim off the "id: " at the front
    else:
        user_id = 0
    user_info['id'] = user_id

    #get email_address
    email_address = "none@none.com"
    p = re.compile('[\w\.-]+@[\w\.-]+')
    m = p.findall(str(soup))
    if len(m) > 0:
        if ("@" in m[0]) and ("." in m[0]):
            email_address = m[0]
    else:
        email_address = "none@none.com"
    user_info['email_address'] = email_address

    #get is_active
    is_active = 0
    p = re.compile('(?:is_active: )\w+')
    m = p.findall(str(soup))
    if len(m) > 0:
        if ("is_active" in m[0]) and len(m[0]) == 12:
            is_active = m[0]
            is_active = is_active[11:] #trim off the "is_active: " at the front
    else:
        is_active = 0
    user_info['is_active'] = is_active

    #get last_paid_on
    last_paid_on = 0
    p = re.compile('\d{4}-\d{2}-\d{2}')
    m = p.findall(str(soup))
    if len(m) > 0:
        if (len(m[0]) >= 10):
            last_paid_on = m[0]
    else:
        last_paid_on = 0
    user_info['last_paid_on'] = last_paid_on

    return user_info

if __name__ == "__main__":
    getUserInfo("http://www.nathanwailes.com/autorespond/get-user-info.php", "john.ahn.1987@gmail.com")
    getEmailAddressFromCraigslistReplyPage("http://washingtondc.craigslist.org/nva/ofc/4493158074.html")