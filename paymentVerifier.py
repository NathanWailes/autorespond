from datetime import datetime
from webScrapingPython3 import getUserInfo
#import easygui as eg

class PaymentVerifier:

    def __init__(self):
        self.date_last_checked_that_user_paid = datetime(year=2000, month=1, day=1, hour=1) #an initial value that will be updated
        self.verification_frequency_in_days = 1

    def verifyPayment(self, username):
        time_delta = datetime.now() - self.date_last_checked_that_user_paid
        if (time_delta.days > self.verification_frequency_in_days):
            if not self.hasUserPaid(username):
                return False
            else:
                self.date_last_checked_that_user_paid = datetime.now()
                return True

    def hasUserPaid(self, username):
        user_info = getUserInfo("http://www.craigslistautorespond.com/get-user-info.php", username)

        if int(user_info['is_active']) == 0:
            #print(username + " is_active set to 0.")
            return False

        #If there are no payment entries, last_paid_on will return 0 from the browser
        if user_info['last_paid_on'] == 0:
            return False

        last_paid_on = datetime.strptime(user_info['last_paid_on'], "%Y-%m-%d")
        time_delta = datetime.now() - last_paid_on
        print(username + " last paid on " + str(last_paid_on) + ".")
        if time_delta.days < 31:
            #print(username + " has paid within the last 31 days.")
            return True

        #print(username + " has NOT paid within the last 31 days.")
        return False


def main():
    test_payment_verifier = PaymentVerifier()
    print(test_payment_verifier.verifyPayment("john.ahn.1987@gmail.com"))



if __name__ == "__main__":
    main()


'''
eg.msgbox("This program has reached the conclusion that you haven't paid to use it in the past month.\n\n" \
          "Steps you should follow:\n" \
          "1. Please double-check that you have paid.\n" \
          "2. If you are sure you have paid, try restarting the program to see if that fixes it.\n" \
          "3. If restarting the program doesn't fix the problem and you are sure you have paid, send an email to nathan.wailes@gmail.com.")
'''