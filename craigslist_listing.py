class CraigslistListing:
    def __init__(self, listing_id, region, subregion, title, location, datetime,
                 category):
        self.listing_id = listing_id
        self.region = region
        self.subregion = subregion
        self.title = title
        self.location = location
        self.datetime = datetime
        self.category = category

        self.url = "http://" + self.region + ".craigslist.org/" + \
                                 self.subregion + "/" + self.category + \
                                 self.listing_id + "/"
        self.no_js_listing_url = "http://" + self.region + \
                                 ".craigslist.org/reply/" + \
                                 self.listing_id

        return