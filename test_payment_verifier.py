import unittest
from paymentVerifier import PaymentVerifier

class SimplisticTest(unittest.TestCase):

    def setUp(self):
        self.payment_verifier = PaymentVerifier()

    def test_verify_payment(self):
        username = "inexistent_user@gmail.com"
        self.assertFalse(self.payment_verifier.verifyPayment(username))
        self.assertFalse(self.payment_verifier.hasUserPaid(username))


if __name__ == '__main__':
    unittest.main()