import os
import requests
from utils import createDirIfNecessary
import easygui as eg

class NewUserGenerator:

    email_address = "test4@test.com"
    region = "washingtondc"
    email_body = ""

    def __init__(self):
        self.promptForUserInfo()
        self.submit_new_user_data_to_php_script()
        self.create_new_user_folder()
        return

    def promptForUserInfo(self):
      msg         = ("Enter the user's info.")
      title       = "Enter User Info"
      fieldNames  = ["Email Address", "Region", "Email Body"]
      fieldValues = ["example@gmail.com", "washingtondc", ""]
      fieldValues = eg.multenterbox(msg,title, fieldNames)

      # make sure that none of the fields was left blank
      while 1:  # do forever, until we find acceptable values and break out
          if fieldValues == None: 
              break
          errmsg = ""
          
          # look for errors in the returned values
          for i in range(len(fieldNames)):
              if fieldValues[i].strip() == "":
                errmsg = errmsg + ('"%s" is a required field.\n\n' % fieldNames[i])
              
          if errmsg == "": 
              break # no problems found
          else:
              # show the box again, with the errmsg as the message
              fieldValues = eg.multenterbox(errmsg, title, fieldNames, fieldValues)
      self.email_address = fieldValues[0]
      self.region = fieldValues[1]
      self.email_body = fieldValues[2]
      return

    def submit_new_user_data_to_php_script(self):
        url = "http://www.nathanwailes.com/autorespond/create-new-user.php?" + \
              "email_address=" + self.email_address + "&region=" + self.region
        # spoof some headers so the request appears to be coming from a browser, not a bot
        headers = {
            "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5)",
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "accept-charset": "ISO-8859-1,utf-8;q=0.7,*;q=0.3",
            "accept-encoding": "gzip,deflate,sdch",
            "accept-language": "en-US,en;q=0.8",
        }
        # make the request to the search url, passing in the the spoofed headers.
        r = requests.get(url)  #, headers=headers)assign the response to a variable r
        # check the status code of the response to make sure the request went well
        if r.status_code != 200:
            print("request denied")
            return


    def create_new_user_folder(self):
        new_dir = os.getcwd() + "\\users\\" + self.region + "\\" + self.email_address
        createDirIfNecessary(new_dir)
        return


if __name__ == "__main__":
    new_user = NewUserGenerator()