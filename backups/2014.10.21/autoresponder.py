"""This module decides whether to respond to the list of available ads"""
import feedparser
from datetime import datetime
import re
import time
from sendEmailWithPython3 import sendMail
from webScrapingPython3 import getEmailAddressFromCraigslistReplyPage

def respond_to_new_ads(user):
    """Docstring"""
    rss_feed = feedparser.parse(user.rss_address)
    ads_seen = user.getAdsSeenInPastXDays(7)
    for post in rss_feed.entries:
        if not post.title in ads_seen.keys():
            formatted_title = str(post.title.encode(encoding='utf-8',
                                                    errors='replace'))
            if is_post_of_interest(post, user):
                user.logTheSeenAdvertisement(formatted_title, "Job Hit!")
                respond_to_post(post, user)
                time.sleep(10)
            ads_seen[post.title] = {'published': post.published_parsed}
    user.dumpSeenAds(ads_seen)
    return

def is_post_of_interest(post, user):
    """Docstring"""
    #Step 1: If the post's title has any words that indicate that I won't be
    #interested in the ad, then return "False"
    if ad_ruled_out_by_age(post, user):
        return False
    #if ad_ruled_out_by_phrase("neighborhoods", post, user):
    #    return False
    formatted_title = str(post.title.encode(encoding='utf-8',
                                            errors='replace'))
    neighborhoods_that_rule_out_an_ad = user.neighborhoods_that_rule_out_an_ad
    for word_or_phrase in neighborhoods_that_rule_out_an_ad:
        word_or_phrase = r"\b" + re.escape(word_or_phrase) + r"\b"
        title = clean_up_title(post.title)
        if re.search(word_or_phrase, title):
            #cut off the leading and trailing '\b' for printing out the word
            word_or_phrase = word_or_phrase[2:-2]
            result = "Ruling out this ad because of the word/phrase " + \
                "'{word_or_phrase}' in the title.".format(**locals())
            user.logTheSeenAdvertisement(formatted_title, result)
            return False
    job_descriptions_that_rule_out_an_ad = user.job_descriptions_that_rule_out_an_ad
    for word_or_phrase in job_descriptions_that_rule_out_an_ad:
        word_or_phrase = r"\b" + re.escape(word_or_phrase) + r"\b"
        title = clean_up_title(post.title)
        if re.search(word_or_phrase, title):
            #cut off the leading and trailing '\b' for printing out the word
            word_or_phrase = word_or_phrase[2:-2]
            result = "Ruling out this ad because of the word/phrase " + \
                "'{word_or_phrase}' in the title.".format(**locals())
            user.logTheSeenAdvertisement(formatted_title, result)
            return False
    words_of_interest = user.words_of_interest
    for word_or_phrase in words_of_interest:
        word_or_phrase = r"\b" + re.escape(word_or_phrase) + r"\b"
        title = clean_up_title(post.title)
        if re.search(word_or_phrase, title):
            return True
    return False

def ad_ruled_out_by_age(post, user):
    post_datetime = datetime(
        year=post.published_parsed[0],
        month=post.published_parsed[1],
        day=post.published_parsed[2],
        hour=post.published_parsed[3])
    time_delta = datetime.now() - post_datetime
    formatted_title = str(post.title.encode(encoding='utf-8', errors='replace'))
    if time_delta.days > 1:
        result = "Ruling out this ad because it's {time_delta.days} day(s) " + \
            "old.".format(**locals())
        user.logTheSeenAdvertisement(formatted_title, result)
        return True
    return False

'''
def ad_ruled_out_by_phrase("neighborhoods", post, user):
    for word_or_phrase in neighborhoods_that_rule_out_an_ad:
        word_or_phrase = r"\b" + re.escape(word_or_phrase) + r"\b"
        reformatted_title = post.title.lower()
        reformatted_title = re.sub(r'[^a-zA-Z0-9]', ' ', reformatted_title)
        if re.search(word_or_phrase, reformatted_title):
            #cut off the leading and trailing '\b' for printing out the word
            word_or_phrase = word_or_phrase[2:-2]
            result = "Ruling out this ad because of the word/phrase " + \
                "'{word_or_phrase}' in the title.".format(**locals())
            user.logTheSeenAdvertisement(formatted_title, result)
            return True
    return False
'''
def clean_up_title(post_title):
    reformatted_title = post_title.lower()
    reformatted_title = re.sub(r'[^a-zA-Z0-9]', ' ', reformatted_title)
    return reformatted_title

def respond_to_post(post, user):
    """Docstring"""
    post_email_address = get_craigslist_email_address_for(post)
    if post_email_address == None:
        result = "No easy-to-find email address."
        user.logTheSeenAdvertisement(post.title, result)
        return False
    sender = user.username
    email_subject = post.title
    email_body = user.message.format(**locals())
    recipients = [user.username, post_email_address]
    sendMail(
        sender, recipients, email_subject, email_body, 
        files=user.attachments, server="smtp.gmail.com",
        port=587, username=user.username, password=user.password, isTls=True)
    return False

def get_craigslist_email_address_for(post):
    """Example input: http://washingtondc.craigslist.org/doc/edu/4307056218.html
       Example output: 2vcxb-4604967152@job.craigslist.org"""
    base_url = get_base_craigslist_url_for(post.link)
    ad_number = get_craigslist_ad_number_for(post.link)
    #Combine them into a link where the email will be easy to scrape
    craigslist_reply_link = base_url + "reply/" + ad_number
    post_email_address = getEmailAddressFromCraigslistReplyPage(
        craigslist_reply_link)
    if post_email_address == False:
        return False
    return post_email_address

def get_base_craigslist_url_for(address):
    """Example input: http://washingtondc.craigslist.org/doc/edu/4307056218.html
       Example output: http://washingtondc.craigslist.org/"""
    pattern = re.compile('.*.org/')
    matches = pattern.match(address)
    base_url = matches.group() #includes the slash at the end of the URL
    return base_url

def get_craigslist_ad_number_for(address):
    """Example input: http://washingtondc.craigslist.org/doc/edu/4307056218.html
       Example output: 4307056218"""
    pattern = re.compile('\d+')
    matches = pattern.findall(address)
    ad_number = matches[0]
    return ad_number
