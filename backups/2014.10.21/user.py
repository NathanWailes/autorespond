import json
from GUI import promptForPassword
from datetime import datetime
import time
import os
from utils import createDirIfNecessary
import csv
from paymentVerifier import PaymentVerifier

class User:

  def __init__(self, username, region):
    self.username = username
    self.region = region
    self.password = self.getPassword()
    self.getAndSetUserData()
    self.payment_verifier = PaymentVerifier()

  def getPassword(self):
    try:
      user_settings_path = "users" + "\\" + self.region + "\\" + self.username + "\\" + "settings.json"
      with open(user_settings_path) as f:
        settings = json.load(f)
      user_data = settings[self.username]
      password = user_data["password"]
    except:
      password = promptForPassword(self.username)
    return password

  def getAndSetUserData(self):
    user_settings_path = "users" + "\\" + self.region + "\\" + self.username + "\\" + "settings.json"
    with open(user_settings_path) as f:
      settings = json.load(f)
    user_data = settings[self.username]
    self.rss_address = user_data["rss_address"]
    self.message = user_data["message"]
    attachment_names = user_data["attachments"]
    self.prepareAttachments(attachment_names)
    self.neighborhoods_that_rule_out_an_ad = user_data["neighborhoods_that_rule_out_an_ad"]
    self.job_descriptions_that_rule_out_an_ad = user_data["job_descriptions_that_rule_out_an_ad"]
    self.words_of_interest = user_data["words_of_interest"]
    return

  def prepareAttachments(self, attachment_names):
    self.attachments = []
    for attachment_name in attachment_names:
      full_path_to_attachment = self.addUserDirectoryToFileName(attachment_name)
      self.attachments.append(full_path_to_attachment)
    return

  def getAdsSeenInPastXDays(self, number_of_days):
    ads_seen_filename = "ads_seen.json"
    ads_seen_path = self.addUserDirectoryToFileName(ads_seen_filename)
    with open(ads_seen_path) as f:
      ads_seen_file = json.load(f)
    ads_seen = ads_seen_file[self.username]
    ads_seen = self.cleanOutAdsOlderThanXDays(ads_seen, number_of_days)
    return ads_seen

  def addUserDirectoryToFileName(self, file_name):
    user_path = "users" + "\\" + self.region + "\\" + self.username + "\\"
    full_path = user_path + file_name
    return full_path

  def cleanOutAdsOlderThanXDays(self, ads_seen, cutoff_age_in_days):
    to_be_deleted = []
    for ad, v in ads_seen.items():
      posted_parsed = v["published"]
      post_datetime = datetime(year=posted_parsed[0], month=posted_parsed[1], day=posted_parsed[2], hour=posted_parsed[3])
      time_delta = datetime.now() - post_datetime
      if (time_delta.days > cutoff_age_in_days):
        to_be_deleted.append(ad)
    for ad in to_be_deleted:
      ads_seen.pop(ad)
    return ads_seen

  def dumpSeenAds(self, ads_seen):
    ads_seen_filename = self.addUserDirectoryToFileName("ads_seen.json")
    ads_seen_file = {}
    ads_seen_file[self.username] = ads_seen
    with open(ads_seen_filename, 'w') as f:
      json.dump(ads_seen_file, f)
    return

  def logTheSeenAdvertisement(self, ad_name, message):
    year = time.strftime("%Y")
    month = time.strftime("%m")
    day = time.strftime("%d")
    log_file_dir = os.getcwd() + "\\" + "users" + "\\" + self.region + "\\" + self.username + "\\" + year + "\\" + month
    createDirIfNecessary(log_file_dir)
    output_row = [ad_name, message]
    file_name = day + ".csv"
    with open(os.path.join(log_file_dir, file_name), "a", newline='') as myFile:
      myFileWriter = csv.writer(myFile)
      myFileWriter.writerow(output_row)