import os

def createDirIfNecessary(directory):
  if not os.path.exists(directory):
    os.makedirs(directory)

def createFileIfNecessary(path):
  
  return