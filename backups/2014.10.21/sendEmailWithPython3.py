#Source - http://stackoverflow.com/questions/3362600/how-to-send-email-attachments-with-python

import smtplib, os
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders

def sendMail( send_from, send_to, subject, text, files=[], server="localhost", port=587, username='', password='', isTls=True):
    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime = True)
    msg['Subject'] = subject

    msg.attach( MIMEText(text) )

    for f in files:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(f,"rb").read() )
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="{0}"'.format(os.path.basename(f)))
        msg.attach(part)

    smtp = smtplib.SMTP(server, port)
    if isTls: smtp.starttls()
    smtp.login(username,password)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.quit()

def main():
    message = "Hello,\n\nI'd like be considered for the position. I have attached my resume and references.\n\nNathan Wailes\nwww.nathanwailes.com\n201-316-6159"
    send_mail( "Nathan", ["nathan.wailes@gmail.com"], "Test", message, files=['Wailes, Nathan - Resume.pdf','Wailes, Nathan - References.pdf'], server="smtp.gmail.com", port=587, username='HitchDCSilver@gmail.com', password='easy2remember', isTls=True)
    print("Message sent.")

if __name__ == '__main__':
  main()
