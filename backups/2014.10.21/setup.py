# A simple setup script to create an executable using PyQt4. This also
# demonstrates the method for creating a Windows executable that does not have
# an associated console.
#
# PyQt4app.py is a very simple type of PyQt4 application
#
# Run the build process by running the command 'python setup.py build'
#
# If everything works well you should find a subdirectory in the build
# subdirectory that contains the files needed to run the application

company_name = "Skinner"
product_name = "AutoRespond"
application_title = "AutoRespond" #what you want to application to be called
main_python_file = "AutoRespond.py" #the name of the python file you use to run the program

import sys

from cx_Freeze import setup, Executable

base = None
if sys.platform == "win32":
    base = "Win32GUI"


bdist_msi_options = {
    'upgrade_code': '{66620F3A-DC3A-11E2-B341-002219E9B01E}',
    'add_to_path': False,
    'initial_target_dir': r'[ProgramFilesFolder]\%s\%s' % (company_name, product_name),
    }

build_exe_options = {
    'excludes' : [],
    'includes' : ["tkinter"],
    'packages' : [],
    'include_files' : ["users.json", "options.json", "vcredist_x86 - necessary DLLs for the 32 bit version.exe", "Readme.txt"]
    }

setup(
        name = application_title,
        version = "0.1",
        description = "",
        options = {
          #'bdist_msi': bdist_msi_options,
          'build_exe': build_exe_options},
        executables = [Executable(main_python_file, base = base)])

