from webScrapingPython3 import getUserInfo
from datetime import datetime

username = "nathan.wailes@gmail.com"
user_info = getUserInfo("http://www.nathanwailes.com/autorespond/get-user-info.php", username)
print(type(user_info['is_active']))

last_paid_on = datetime.strptime("2014-07-21", "%Y-%m-%d")

time_delta = datetime.now() - last_paid_on
print(time_delta.days)

#print(datetime.now() - 30)