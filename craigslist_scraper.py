#Source - http://stackoverflow.com/questions/16782726/web-scraping-tutorial-using-python-3

import requests
from bs4 import BeautifulSoup
import re
from utils import emailErrorMessage
from dateutil import parser
from datetime import datetime
from craigslist_listing import CraigslistListing

class CraigslistScraper:
    def __init__(self):
        self.listings = []
        return


    def add_listings(self, listing_region, listing_category):
        url = "http://" + listing_region + ".craigslist.org/search/" + listing_category

        # spoof some headers so the request appears to be coming from a browser, not a bot
        headers = {
            "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5)",
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "accept-charset": "ISO-8859-1,utf-8;q=0.7,*;q=0.3",
            "accept-encoding": "gzip,deflate,sdch",
            "accept-language": "en-US,en;q=0.8",
        }
        # make the request to the search url, passing in the the spoofed headers.
        r = requests.get(url, headers=headers)  #assign the response to a variable r
        # check the status code of the response to make sure the request went well
        if r.status_code != 200:
            print("request denied")
            return

        # convert the plaintext HTML markup into a DOM-like structure that we can search
        soup = BeautifulSoup(r.text)

        #I used http://pythex.org/ to test these regexes
        listing_id_regex = re.compile('[0-9]{10}')
        listing_title_regex = re.compile('(?<=html"\>)[^\<]+(?=\</a>)')
        listing_location_regex = re.compile('(?<=small\> \()[^\<]*(?=\)\</small>)')
        listing_time_regex = re.compile('(?<=datetime=")[^"]*')
        listing_subregion_regex = re.compile('(?<=href="\/)[a-z]{3}(?=\/[a-z]{3})')
        listing_category_regex = re.compile('(?<=href="\/[a-z]{3}\/)[a-z]{3}')

        #site_text = str(soup.encode(encoding='utf-8', errors='replace'))
        #print(site_text)

        number_of_listing_matches = 0
        number_of_title_matches = 0
        number_of_location_matches = 0
        number_of_time_matches = 0
        number_of_subregion_matches = 0
        number_of_listing_category_matches = 0

        for span in soup.find_all('p'):
            span_text = str(span.encode(encoding='utf-8', errors='replace'))
            #print(span_text)

            m = listing_id_regex.findall(str(span))

            if len(m) > 1:
                listing_id = m[0]
                number_of_listing_matches += 1
                listing_title = ""
                listing_location = ""
                listing_datetime = datetime(1900, 1, 1)
                listing_subregion = ""
                listing_category = ""

                #get the title of the listing
                m = listing_title_regex.findall(span_text)
                if len(m) > 0:
                    listing_title = m[0]
                    number_of_title_matches += 1

                #get the location of the listing
                m = listing_location_regex.findall(span_text)
                if len(m) > 0:
                    listing_location = m[0]
                    number_of_location_matches += 1
                
                #get the time of the listing
                m = listing_time_regex.findall(span_text)
                if len(m) > 0:
                    listing_time_str = m[0]
                    listing_datetime = parser.parse(listing_time_str)
                    number_of_time_matches += 1

                #get the subregion of the listing
                #Examples: the doc / nva / mld subregions
                #in the washingtondc region
                m = listing_subregion_regex.findall(span_text)
                if len(m) > 0:
                    listing_subregion = m[0]
                    number_of_subregion_matches += 1
                
                #get the job category of the listing
                #Example: hea, spa, ret, sls
                m = listing_category_regex.findall(span_text)
                if len(m) > 0:
                    listing_category = m[0]
                    number_of_listing_category_matches += 1
                
                new_listing = CraigslistListing(listing_id,
                                                listing_region,
                                                listing_subregion,
                                                listing_title,
                                                listing_location,
                                                listing_datetime,
                                                listing_category)
                #print([new_listing.subregion, new_listing.job_category])
                self.listings.append(new_listing)

        if number_of_listing_matches == 0 or \
            number_of_title_matches == 0 or \
            number_of_location_matches == 0 or \
            number_of_time_matches == 0 or \
            number_of_subregion_matches == 0 or \
            number_of_listing_category_matches == 0:
            emailErrorMessage("Something's wrong with the regexes in craigslist_scraper.py")

        return

    def get_regex_match(self, regex, span_text):
        m = regex.findall(span_text)
        if len(m) > 0:
            match = m[0]
        return match

    def get_listings(self):

        return self.listings
if __name__ == "__main__":

    # you can pass in a keyword to search for when you run the script
    # by default, we'll search for the "web scraping" keyword
    '''
    try:
        keyword = sys.argv[1]
    except IndexError:
        keyword = "web scraping"
    '''
    #print(getUserInfo("http://www.nathanwailes.com/autorespond/get-user-info.php", "john.ahn.1987@gmail.com"))
    #getEmailAddressFromCraigslistReplyPage("http://washingtondc.craigslist.org/nva/ofc/4493158074.html")
    craigslist_scraper = CraigslistScraper()
    craigslist_scraper.add_listings("washingtondc", "jjj")
    print(craigslist_scraper.listings)