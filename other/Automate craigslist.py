# http://jayrambhia.com/blog/send-emails-using-python/
#!/usr/bin/python2.7
import smtplib #prob. need this
import imaplib
import getpass
import email
import pickle
import base64 #need this
import os
from email.MIMEMultipart import MIMEMultipart
from email.Utils import COMMASPACE
from email.MIMEBase import MIMEBase
from email.parser import Parser
from email.MIMEImage import MIMEImage
from email.MIMEText import MIMEText
from email.MIMEAudio import MIMEAudio
import mimetypes

def main():
  user = "nathan.wailes"#raw_input('Username: ')
  passw = base64.b64encode(getpass.getpass())
  
  smtp_host = 'smtp.gmail.com'
  smtp_port = 587
  server = smtplib.SMTP()
  server.connect(smtp_host,smtp_port)
  server.ehlo()
  server.starttls()
  server.login(user,base64.b64decode(passw))
  
  imap_host = 'imap.gmail.com'
  mail = imaplib.IMAP4_SSL(imap_host)
  mail.login(user,base64.b64decode(passw))
  n=42
  while n != 0:
    n = get_int('1. Send email\n2. Get email\n0. Exit\nChoose an option: ') 
    if n==1:
      send_mail(user,server)
    elif n==2:
      get_mail(mail)
    elif n==0: 
      server.quit()
      mail.logout()
  return



if __name__ == '__main__':
  main()
