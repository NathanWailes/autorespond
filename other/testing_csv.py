import csv
import os
myPath = "C:\\"
myRatings = [ ["Movie", "Rating"],
              ["Rebel Without a Cause", "3"],
              ["Monty Python's Life of Brian", "5"],
              ["Santa Claus Conquers the Martians", "0"] ]
with open(os.path.join(myPath, "movies.csv"), "w", newline='') as myFile:
  myFileWriter = csv.writer(myFile)
  myFileWriter.writerows(myRatings)