#Source: http://www.blog.pythonlibrary.org/2012/06/08/python-101-how-to-submit-a-web-form/

import requests
import webbrowser
from requests.auth import HTTPDigestAuth

url = "http://www.artofproblemsolving.com/Wiki/index.php?title=1986_AIME_Problems/Problem_1&amp;action=submit"
payload = {'wpTextbox1':'python'}
r = requests.post(url, data=payload, auth=HTTPDigestAuth('nathan_wailes', 'judyryan'))
print r.status_code
#with open("requests_results.html", "w") as f:
#    f.write(r.content)

#webbrowser.open("results.html")
