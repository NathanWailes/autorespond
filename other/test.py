from datetime import datetime
import json

ads_seen_filename = "ads_seen_test.json"
with open(ads_seen_filename) as f:
  ads_seen = json.load(f)

#clean out old ads
to_be_deleted = []
for ad, v in ads_seen.items():
  posted_parsed = v["published"]
  post_datetime = datetime(year=posted_parsed[0], month=posted_parsed[1], day=posted_parsed[2], hour=posted_parsed[3])
  time_delta = datetime.now() - post_datetime
  delta_hours = time_delta.seconds // 3600
  if (delta_hours > 10):
  	to_be_deleted.append(ad)
for ad in to_be_deleted:
	ads_seen.pop(ad)

with open(ads_seen_filename, 'w') as f:
  json.dump(ads_seen, f)