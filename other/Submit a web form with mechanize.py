#Source: http://www.blog.pythonlibrary.org/2012/06/08/python-101-how-to-submit-a-web-form/

import mechanize
import webbrowser
 
url = "http://duckduckgo.com/html"
br = mechanize.Browser()
br.set_handle_robots(False) # ignore robots
br.open(url)
br.select_form(name="x")
br["q"] = "python"
res = br.submit()
content = res.read()
with open("mechanize_results.html", "w") as f:
    f.write(content)
 
webbrowser.open("results.html")
