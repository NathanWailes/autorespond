#Source: http://www.blog.pythonlibrary.org/2012/06/08/python-101-how-to-submit-a-web-form/

import urllib
import urllib2
import webbrowser
 
url = "http://duckduckgo.com/html"
data = urllib.urlencode({'q': 'Python'})
results = urllib2.urlopen(url, data)
with open("results.html", "w") as f:
    f.write(results.read())
 
webbrowser.open("results.html")
