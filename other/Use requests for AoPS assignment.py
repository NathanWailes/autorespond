
#Has an MAA notice:
#http://www.artofproblemsolving.com/Wiki/index.php?title=2001_AMC_12_Problems/Problem_8&action=edit

#Does not have an MAA notice:
#http://www.artofproblemsolving.com/Wiki/index.php?title=1986_AIME_Problems/Problem_1&action=edit

#MAA notice:
#{{MAA Notice}}

def main():
  from bs4 import BeautifulSoup
  import urllib2
  import re
  myAddress = "http://www.artofproblemsolving.com/Wiki/index.php?title=2001_AMC_12_Problems/Problem_8&action=edit"
  htmlPage = urllib2.urlopen(myAddress)
  htmlText = htmlPage.read()
  mySoup = BeautifulSoup(htmlText)
  textarea_array = mySoup.findAll(attrs={'id' : "wpTextbox1"})
  textarea = str(textarea_array[0])
  p = re.compile('{{MAA Notice}}')
  if (p.search(textarea)):
      print "There's a match!"
  return



if __name__ == '__main__':
  main()
