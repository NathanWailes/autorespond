#Useful for using Feedparser - http://www.pythonforbeginners.com/python-on-the-web/using-feedparser-in-python/
#Format for the pages w/ the craigslist email addresses - http://washingtondc.craigslist.org/reply/4303104829
#For sending the emails - http://www.magiksys.net/pyzmail/

#staffing company websites to watch:
#http://excaliburstaffing.com/career/
#http://keepersstaffing.com/

#!/usr/bin/env python
import sys
import feedparser # pip install feedparser
import time #to have the program wait before looping
import json #to store the ads we've seen in a file
from sendEmailWithPython3 import sendMail
from webScrapingPython3 import getEmailAddressFromCraigslistReplyPage
import re #I'm using this to extract a number from the CL URL that I can then use to get the CL email address
import easygui as eg
from datetime import datetime #I use this in my print statements to know when they were printed out (eg I'll know when I applied for a job)

def main():
  email_info = inputEmailInfo()
  username = email_info[0]
  password = email_info[1]

  while True:

    with open('profiles.json') as f:
      profiles = json.load(f)
    current_user = profiles[username]

    rss_address = current_user["rss_address"]
    rss_feed = feedparser.parse(rss_address)

    ads_seen_filename = current_user["ads_seen_json"]
    with open(ads_seen_filename) as f:
      ads_seen = json.load(f)
    ads_seen = cleanOutAdsOlderThanXDays(ads_seen, 1)

    for post in rss_feed.entries:
      if not (post.title) in ads_seen.keys():
        if isPostOfInterest(post, current_user):
          formatted_title = str(post.title.encode(encoding=sys.stdout.encoding,errors='replace'))
          print("Job hit! - " + formatted_title)
          respondToPost(post, username, password, current_user["message"], current_user["attachments"])
          time.sleep(5)
        ads_seen[post.title] = {'published': post.published_parsed}

    with open(ads_seen_filename, 'w') as f:
      json.dump(ads_seen, f)

    current_hour_and_minute = datetime.now().strftime('%H:%M')
    print("Waiting a minute. (" + current_hour_and_minute + ")")
    time.sleep(60)

def inputEmailInfo():
  msg = "Enter logon information"
  title = "Username and Password"
  fieldNames = ["Username", "Password"]
  fieldValues = ["johnahn1987@gmail.com", "easy2remember"]  # we start with blanks for the values
  fieldValues = eg.multpasswordbox(msg,title, fieldNames, fieldValues)
  # make sure that none of the fields was left blank
  while 1:
    if fieldValues == None: break
    errmsg = ""
    for i in range(len(fieldNames)):
      if fieldValues[i].strip() == "":
        errmsg = errmsg + ('"%s" is a required field.\n\n' % fieldNames[i])
    if errmsg == "": 
        break # no problems found
    else:
      # show the box again, with the errmsg as the message
      fieldValues = eg.multpasswordbox(errmsg, title, fieldNames, fieldValues)
  return fieldValues

def cleanOutAdsOlderThanXDays(ads_seen, cutoff_age_in_days):
  to_be_deleted = []
  for ad, v in ads_seen.items():
    posted_parsed = v["published"]
    post_datetime = datetime(year=posted_parsed[0], month=posted_parsed[1], day=posted_parsed[2], hour=posted_parsed[3])
    time_delta = datetime.now() - post_datetime
    difference_in_days = time_delta.seconds // (3600 * 24)
    if (difference_in_days > cutoff_age_in_days):
      to_be_deleted.append(ad)
  for ad in to_be_deleted:
    ads_seen.pop(ad)
  return ads_seen

def isPostOfInterest(post, current_user):
  post_datetime = datetime(year=post.published_parsed[0], month=post.published_parsed[1], day=post.published_parsed[2], hour=post.published_parsed[3])
  time_delta = datetime.now() - post_datetime
  formatted_title = str(post.title.encode(encoding=sys.stdout.encoding,errors='replace'))
  if time_delta.days > 1:
    print("Ruling out this ad because it's {time_delta.days} day(s) old: ".format(**locals()) + formatted_title)
    return False
  #Step 1: If the post's title has any words that indicate that I won't be interested in the ad, then return "False"
  neighborhoods_that_rule_out_an_ad = current_user["neighborhoods_that_rule_out_an_ad"]
  for word_or_phrase in neighborhoods_that_rule_out_an_ad:
    word_or_phrase = r"\b" + re.escape(word_or_phrase) + r"\b"
    reformatted_title = post.title.lower()
    reformatted_title = re.sub(r'[^a-zA-Z0-9]',' ', reformatted_title)
    if re.search(word_or_phrase, reformatted_title):
      word_or_phrase = word_or_phrase[2:-2] #cut off the leading and trailing '\b' for when we print out the word
      print("Ruling out this ad because of the word/phrase '{word_or_phrase}' in the title: ".format(**locals()) + formatted_title)
      return False
  job_descriptions_that_rule_out_an_ad = current_user["job_descriptions_that_rule_out_an_ad"]
  for word_or_phrase in job_descriptions_that_rule_out_an_ad:
    word_or_phrase = r"\b" + re.escape(word_or_phrase) + r"\b"
    reformatted_title = post.title.lower()
    reformatted_title = re.sub(r'[^a-zA-Z0-9]',' ', reformatted_title)
    if re.search(word_or_phrase, reformatted_title):
      word_or_phrase = word_or_phrase[2:-2] #cut off the leading and trailing '\b' for when we print out the word
      print("Ruling out this ad because of the word/phrase '{word_or_phrase}' in the title: ".format(**locals()) + formatted_title)
      return False
  words_of_interest = current_user["words_of_interest"]
  for word_or_phrase in words_of_interest:
    word_or_phrase = r"\b" + re.escape(word_or_phrase) + r"\b"
    reformatted_title = post.title.lower()
    reformatted_title = re.sub(r'[^a-zA-Z0-9]',' ', reformatted_title)
    if re.search(word_or_phrase, reformatted_title):
      return True
  return False

def respondToPost(post, username, password, message, attachments):
  post_email_address = getEmailAddressFor(post)
  if post_email_address == None:
    print("No easy-to-find email address.")
    return False
  message = message.format(**locals())
  sendMail( username, [username, post_email_address], post.title, message, files=attachments, server="smtp.gmail.com", port=587, username=username, password=password, isTls=True)
  return False

def getEmailAddressFor(post):
  #print("Post link:" + post.link) #post.link example: http://washingtondc.craigslist.org/doc/edu/4307056218.html
  #extract base URL from post link, for example, http://washingtondc.craigslist.org/
  p = re.compile('.*.org/')
  m = p.match(post.link)
  base_url = m.group() #INCLUDES THE SLASH AT THE END
  #extract ad number from post link, for example, 4307056218
  p = re.compile('\d+')
  m = p.findall(post.link)
  ad_number = m[0]
  #combine them into a CL-reply link, for example, http://washingtondc.craigslist.org/reply/4307056218
  craigslist_reply_link = base_url + "reply/" + ad_number
  post_email_address = getEmailAddressFromCraigslistReplyPage(craigslist_reply_link)
  if post_email_address == False:
    return False
  #go to that reply link and extract the email address listed
  return post_email_address

if __name__ == "__main__":
  main()
