

jjj
acc
ofc
egr
med
sci
bus
csr
edu
fbh
lab
gov
hum
eng
lgl
mnu
mar
hea
npo
rej
ret
sls
spa
sec
trd
sof
sad
tch
trp
tfr
web
wri
etc

















***HIGHER PRIORITY***

1. Allow people to get themselves set up via the website
PayPal
 - switch to the real version of PayPal
 - test that the real version of PayPal works as it should

Python Program
 - remove references to "is_active"
 - update how it calculates if someone's subscription is active

Response Profiles
Attachments
 - get it so that people can upload files and they'll be stored in the MySQL database
 - get the Python program to download any new files on the DB

 - decide if you want users creating their own profile or if you want to autocreate one for them.


2. Update the desktop comp to use the new version of the program





***MID PRIORITY***

 - get a daily email notification of the disabled users and active users.
 - get a daily summary of the number of queries to CL, the DB, and emails sent to gmail

 - count the total number of queries we make to the AutoRespond DB
 - count the total number of queries we make to craigslist
 - count the total number of emails we send out

1. You need to make sure the options.json entry matches the profiles.json; handle the error if it doesn't.
2. You need to make sure the ads_seen.json matches the profiles.json; handle the error if it doesn't (make a new entry in ads_seen?).
 - allow people to have multiple response profiles





***LOWER PRIORITY***

 - Allow people to turn response profiles on and off

 - use this app to send SMS from the computer:
http://mightytext.net/#featurewrap

 - Create some kind of notification to the user that the program is working properly once they sign in.

 - have some way to know which version of the program people are running. Maybe have it as a field in the MySQL database. This will allow you to force people to upgrade if they're using a version of the program that you don't want them to use (eg if there's an exploit in it).

 - prevent people from running the program multiple times on the same computer. Otherwise they'll be able to get around the 30-emails-per-day limit.

 - break the "create a profile" GUI into three separate parts:
1) insert the email address that the invitation was given to.
2) Ask the person what RSS address(es) they would like to use (or prompt them 
3) Ask them to type out the message they want to use.

 - create a taskbar icon

 - create an msi (installer)

 - figure out how to deal with uninstalling / reinstalling if you release later versions. What will happen to people's jsons, for example, if you release a new version of the program and someone tries to install it?

 - create a python program to automatically add rows to the MySQL database given a list of email addresses or phone numbers.


Steps for the program to follow:
1. User starts the program.
2. Program checks a file for a saved user-email/ID.
3. If no save one, it asks person for an email/ID.
4. It then queries the db and makes sure such a user exists and is active.
5. If so, it uses that email address to send out emails. [This is what will prevent people from sharing the program.]

 - make the program create a daily report of ads it looked at, ads it ruled out, and ads it selected

 - I need to handle errors when people put in their email address wrong and the php query comes back with nothing

#staffing company websites to watch:
#http://excaliburstaffing.com/career/
#http://keepersstaffing.com/







****DONE****

DONE - switch from a last_paid column to a subscription_expires_on column
DONE - remove the is_active column
DONE - have the program log ads seen to a folder called "logs", with each day being a different text file, and with records of whether the ad was discarded or applied to, and why
DONE - set up recurring payments
DONE - make the program check online to make sure the user is a paying customer
DONE - have the program query the database and check the last time it paid, and if it was more than a month it should send a request to update the person to no longer be active, and should pop up a message telling the user that he hasn't paid in a month.












Top 20 US Metro Areas by Population
(use this, not the top 20 cities listed below)
1. NYC
2. LA
3. Chicago
4. Dallas-Fort Worth
5. Philadelphia
6. Houston
7. Washington, DC
8. Miami (West Palm Beach) - busy
9. Atlanta
10. Boston
11. SF-Oakland
12. Detroit
13. Riverside-San Bernardino
14. Phoenix
15. Seattle
16. Minneapolis-St. Paul
17. San Diego
18. St. Louis
19. Tampa-St. Petersburg
20. Baltimore

Top 24 US Cities by Population
1. NYC
2. LA
3. Chicago
4. Houston
5. Philadelphia
6. Phoenix
7. San Antonio
8. San Diego
9. Dallas
10. San Jose
11. Austin - busy
12. Indianapolis - busy
13. Jacksonville - light
14. San Francisco - very busy
15. Columbus - very moderate
16. Charlotte - moderate
17. Fort Worth - busy
18. Detroit - moderate
19. El Paso - dead
20. Memphis - moderate
21. Seattle - busy
22. Denver - busy
23. Washington, DC - busy
24. Boston - busy





