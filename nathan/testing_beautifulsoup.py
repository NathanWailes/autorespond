#Source - http://stackoverflow.com/questions/16782726/web-scraping-tutorial-using-python-3

import sys
import requests
from bs4 import BeautifulSoup
import re


def getListedCraigslistJobs(url):

    # spoof some headers so the request appears to be coming from a browser, not a bot
    headers = {
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5)",
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "accept-charset": "ISO-8859-1,utf-8;q=0.7,*;q=0.3",
        "accept-encoding": "gzip,deflate,sdch",
        "accept-language": "en-US,en;q=0.8",
    }
    # make the request to the search url, passing in the the spoofed headers.
    r = requests.get(url)  #, headers=headers)assign the response to a variable r
    # check the status code of the response to make sure the request went well
    if r.status_code != 200:
        print("request denied")
        return

    # convert the plaintext HTML markup into a DOM-like structure that we can search
    soup = BeautifulSoup(r.text)

    #I used http://pythex.org/ to test these regexes
    listing_id_regex = re.compile('[0-9]{10}')
    listing_title_regex = re.compile('(?<=html"\>)[^\<]+(?=\</a>)')
    listing_location_regex = re.compile('(?<=small\>)[^\<]*(?=\</small>)')

    #site_text = str(soup.encode(encoding='utf-8', errors='replace'))
    #print(site_text)
    for span in soup.find_all('p'):
        span_text = str(span.encode(encoding='utf-8', errors='replace'))
        #print(span_text)
        m = listing_id_regex.findall(str(span))


        '''
        The page source has two kinds of links for each ad:
        1. Example: <a class="i" href="/mld/trp/4724324950.html"></a>
        2. Example: <a class="hdrlnk" data-id="4724324950" href="/mld/trp/4724324950.html">Courrier Position MD/DC metro area</a>
        Since only the second kind of link has the title of the ad, that's the
        one we want to look for the title in.
        '''
        if len(m) > 1:
            listing_id = m[0]
            listing_title = ""
            listing_location = ""

            #get the title of the listing
            m = listing_title_regex.findall(span_text)
            if len(m) > 0:
                listing_title = m[0]
                #print([listing_title, listing_id])


            #get the location of the listing
            m = listing_location_regex.findall(span_text)
            if len(m) > 0:
                listing_location = m[0]
            
            print([listing_title, listing_id, listing_location])
            '''
        for match in m:
            print(match)
        for match in n:
            print(match)
        '''

if __name__ == "__main__":

    # you can pass in a keyword to search for when you run the script
    # by default, we'll search for the "web scraping" keyword
    '''
    try:
        keyword = sys.argv[1]
    except IndexError:
        keyword = "web scraping"
    '''
    #print(getUserInfo("http://www.nathanwailes.com/autorespond/get-user-info.php", "john.ahn.1987@gmail.com"))
    #getEmailAddressFromCraigslistReplyPage("http://washingtondc.craigslist.org/nva/ofc/4493158074.html")
    print(getListedCraigslistJobs("http://washingtondc.craigslist.org/search/jjj/"))
