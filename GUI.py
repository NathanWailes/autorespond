import json
import easygui as eg
from utils import createDirIfNecessary


def createProfileIfNecessary():
  with open('users.json') as f:
    users_by_region = json.load(f)
  try:
    for region in users_by_region:
      if len(users_by_region[region]) > 0:
        return
  except:
    print("Error!")
  msg = ("The program isn't detecting an existing profile for you. To continue "
          "you'll need to create a profile. Do you want to continue?")
  title = "Please Confirm"
  if eg.ccbox(msg, title):     # show a Continue/Cancel dialog
      pass  # user chose Continue
  else:
      sys.exit(0)           # user chose Cancel

  username = promptForEmailAddress()

  new_profile = {}
  new_profile["rss_address"] = promptForRSSPreferences()
  #new_profile["message"] = fieldValues[2]

  cover_letter_path = navigateToAttachment("Cover letter")
  resume_path = navigateToAttachment("Resume")
  references_path = navigateToAttachment("References")
  new_profile["attachments"] = [resume_path, cover_letter_path, references_path]

  new_profile["neighborhoods_that_rule_out_an_ad"] = {}
  new_profile["job_descriptions_that_rule_out_an_ad"] = {}
  new_profile["words_of_interest"] = {}

  #save the new profile to the json
  profiles = {}
  profiles[username] = new_profile

  user_settings_dir = "users" + "\\" + region + "\\" + username 
  createDirIfNecessary(user_settings_dir)
  user_settings_path = user_settings_dir + "\\" + "settings.json"
  with open(user_settings_path, 'w') as f:
    json.dump(profiles, f)

  #update default profile
  with open('options.json') as f:
    options = json.load(f)
  options["default_profile"] = username
  with open('options.json', 'w') as f:
    json.dump(options, f)




def promptForEmailAddress():
  msg         = ("What is your email address? Note: This should be the same "
                "email address that the invitation to use this program was sent"
                " to.")
  title       = "Enter Email Address"
  fieldNames  = ["Email Address"]
  fieldValues = ["example@gmail.com"]  # we start with blanks for the values
  fieldValues = eg.multenterbox(msg,title, fieldNames)

  # make sure that none of the fields was left blank
  while 1:  # do forever, until we find acceptable values and break out
      if fieldValues == None: 
          break
      errmsg = ""
      
      # look for errors in the returned values
      for i in range(len(fieldNames)):
          if fieldValues[i].strip() == "":
            errmsg = errmsg + ('"%s" is a required field.\n\n' % fieldNames[i])
          
      if errmsg == "": 
          break # no problems found
      else:
          # show the box again, with the errmsg as the message
          fieldValues = eg.multenterbox(errmsg, title, fieldNames, fieldValues)
  email_address = fieldValues[0]
  return email_address

def promptForRSSPreferences():
  state_preference = getStatePreference()
  rss_address = "None"
  return rss_address

def getStatePreference():
  msg     = "Which Craigslist site are you interested in?"
  title   = "Choose Craigslist Region"
  choices = ["Alabama", "Alaska", "Arizona", "Arkansas", "California",
             "Colorado", "Connecticut", "Delaware", "District of Columbia",
             "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana",
             "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland",
             "Massachusetts", "Michigan", "Minnesota", "Mississippi", 
             "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire",
             "New Jersey", "New Mexico", "New York", "North Carolina", 
             "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania",
             "Rhode Island", "South Carolina", "South Dakota", "Tennessee",
             "Texas", "Utah", "Vermont", "Virginia", "Washington",
             "West Virginia", "Wisconsin", "Wyoming", "Territories"]
  while 1:
    choice   = eg.choicebox(msg, title, choices)
    if choice == "District of Columbia":
      break
    else:
      eg.msgbox("That state is currently not supported.")

  craigslist_region_prefix = getCraigslistRegionPrefix(choice)

  return craigslist_region_prefix

def getCraigslistRegionPrefix(state_name): #expand this later
  if state_name == "Alabama":
    return "washingtondc"
  if state_name == "Alaska":
    return "washingtondc"
  if state_name == "Arizona":
    return "washingtondc"
  if state_name == "Arkansas":
    return "washingtondc"
  if state_name == "California":
    return "washingtondc"
  if state_name == "Colorado":
    return "washingtondc"
  if state_name == "District of Columbia":
    return "washingtondc"
  return False

def navigateToAttachment(attachment_type):
  msg = ("In the next screen please select your " + attachment_type.upper() + 
          ". Keep in mind that if you move or rename it the program won't work!")
  eg.msgbox()
  title = "Select " + attachment_type.title()
  path_to_attachment = eg.fileopenbox(msg="", title=title , default='*', filetypes=None)
  confirmation_message = attachment_type + " path saved. Keep in mind that if you move or rename it the program won't work!"
  eg.msgbox(confirmation_message)
  return path_to_attachment


def promptForPassword(username):
  msg = "Please enter the password for " + username + ". This program needs it to be able to send emails on your behalf. This program does not store your password."
  title = "Enter Password"
  fieldNames = ["Password"]

  fieldValues = [""]  # we start with blanks for the values
  fieldValues = eg.passwordbox(msg=msg,title=title)
  # make sure that none of the fields was left blank
  while 1:
    if fieldValues == None: break
    errmsg = ""
    for i in range(len(fieldNames)):
      if len(fieldValues) == 0:
        errmsg = errmsg + ('"%s" is a required field.\n\n' % fieldNames[i])
    if errmsg == "": 
        break # no problems found
    else:
      # show the box again, with the errmsg as the message
      fieldValues = eg.passwordbox(msg=errmsg,title=title)
  return fieldValues



def selectNeighborhoodsThatRuleOutAnAd():
  neighborhoods_that_rule_out_an_ad = {}
  while True:
    msg = 'This window deals with NEIGHBORHOODS that rule out an ad. Would you like to add/remove any words or phrases?'

    choice = indexbox(msg=msg, title='Neighborhoods That Rule Out an Ad', choices=( 'Continue', 'Add Word/Phrase', 'Delete Word/Phrase'), image=None)
    if choice == 0:
      break
  return neighborhoods_that_rule_out_an_ad
